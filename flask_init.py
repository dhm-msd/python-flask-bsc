from flask import Flask, render_template
from flask_pymongo import PyMongo
app = Flask(__name__)


@app.route("/login")
def login_page():
    return render_template('login.html', title="Login Page")


@app.route("/")
def index_page():
    return render_template('index.html', title="Index Page")
